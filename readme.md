# Dungeon Text Crawler.

Try to escape the Dungeon by navigating through the rooms!

The Character is controlled by text input, and there are six rooms in total.

Valid user input is:
'north' - 'east' - 'south' - 'west'

### User Feedback:
-----------------------------------------------------
Subject: *I'm stuck!*

Description: *I am unable to escape the first two rooms in the game, and it's SO annoying.*

-

Subject: *Game will not start*

Description:*Everytime I try to run the game, it crashes and says something about syntax error or something...I don't know.*

-

Subject: *The game is too easy*

Description: *I can beat this in my sleep! Can't the developers come up with anything better?*

-

Subject: *Nothing happens when I type 'w'*

Description:*When I type anything other the entire direction name ie: 'west' - nothing happens?  What is going on in the game here?*

-

Subject: *How to play?....*

Description:*I am unsure how to stat the game... are there instructions somewhere?*

-
